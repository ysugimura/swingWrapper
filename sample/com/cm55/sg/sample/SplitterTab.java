package com.cm55.sg.sample;

import com.cm55.sg.*;

public class SplitterTab {

  SgSplitPane component;
  
  public SplitterTab() {
    
    component = new SgSplitPane.H(
      new SgGroupBox("A", new SgFlowLayout(new SgLabel("test"))),
      new SgGroupBox("B", new SgFlowLayout(new SgLabel("sample")))
    );
    
  }

}
