package com.cm55.sg.sample;

import com.cm55.sg.*;

public class TextAreaTab {

  SgPanel component;
  SgTextArea textArea;
  
  public TextAreaTab() {
    component = new SgPanel(new SgBorderLayout.V(
      null,
      new SgScrollPane(textArea = new SgTextArea()),
      new SgPanel(new SgFlowLayout(
        new SgButton("テキスト追加", this::appendText),
        new SgButton("クリア", e->textArea.setText(""))
      ))
    ));
    
    new SgPopupMenu(
      new SgMenuItem("コピー", i->textArea.copy()), 
      new SgMenuItem("貼り付け", i->textArea.paste())
    ).attachOn(textArea);    
  }
  
  private void appendText(SgButton button) {
    textArea.append("この記事もMarkdownを使って書いています。変換前のテキストを以下に用意したので、参考にしてください。\r\n" + 
        "\r\n" + 
        "このプレーンテキストが一発でこの記事のHTMLに変換されています。サイト更新が非常に楽になるので、ブロガーさんや手書きHTMLのサイトを運営してる方などはぜひ一度試してみてください。");
  }

}
