package com.cm55.sg.sample;

import java.awt.*;

import com.cm55.sg.*;

public class SampleMain {

  private SgFrame frame;
  SgTabbedPane tabbedPane;
  
  SampleMain() {
    frame = new SgFrame("Sample")
 
      .setSize(400, 400)
      .setLayout(new BorderLayout())
      .setLayout(new SgBorderLayout.V(
        new SgLabel("これはサンプルのラベルです")
          .setBorder(SgBorder.createPadding(10)),
        tabbedPane = new SgTabbedPane()
          .addTab("ボタン", new ButtonsTab().component)
          .addTab("テキストエリア", new TextAreaTab().component)
          .addTab("テーブル", new TableTab().component)
          .addTab("スプリッタ", new SplitterTab().component)
          .addTab("ボーダーレイアウト", new BorderLayoutTab().component)
          .addTab("フォーム",  new FormTab().component)
          .setSelectionHandler((o, n)->System.out.println("change " + o + "," + n))
          ,
        null
      ))
      .setMenuBar(new SgMenuBar(
        new SgMenu("File", 
          new SgMenuItem("test", e->tabbedPane.select(1)),
          new SgMenu("Cascaded", 
            new SgMenuItem("one", this::clicked),
            new SgMenuItem("two", this::clicked)
          )
          
        ),
        LafControl.getMenu()
      ))
      .setVisible(true);
  }

  private void clicked(SgMenuItem item) {
   SgDialogs.showMessage(frame, "アイテム",  "" + item.getText());
    
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    double width = screenSize.getWidth();
    double height = screenSize.getHeight();
    System.out.println("bounds " + frame.getBounds() + "," + width + "," + height);
  }
  
  public static void main(String[] args) throws Exception {      
    LafControl.initialize();
    new SampleMain();
  }
}
