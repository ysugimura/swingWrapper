package com.cm55.sg.sample;

import com.cm55.sg.*;

public class FormTab {

  SgComponent<?>component;

  
  public FormTab() {
    SgForm.Builder builder = new SgForm.Builder();
    builder.add("ユーザ名",  new SgTextField().setEnterHandler(e->System.out.println(e)));
    builder.add("パスワード",  new SgTextField());
    component = builder.build();
    
  }

}
