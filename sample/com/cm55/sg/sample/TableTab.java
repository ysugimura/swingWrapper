package com.cm55.sg.sample;

import java.util.*;

import com.cm55.sg.*;
import com.cm55.sg.SgListSelectionModel.*;

public class TableTab {
  
  SgPanel component;
  SgTable<Addr> table;
  SgListSelectionModel selectionModel;
  
  public TableTab() {

    List<Addr>list = new ArrayList<>();
    for (int i = 0; i < 100; i++) {
      list.add(new Addr("鈴木", "88889999"));
      list.add(new Addr("佐藤", "00998899"));
    }
    
    @SuppressWarnings("unchecked")
    SgColumn<Addr, ?>[]columns = new SgColumn[] {
      new SgTextColumn<Addr>("名前", addr->addr.name).setPreferredWidth(200),
      new SgTextButtonColumn<Addr>("電話",  addr->addr.phone)
        .setOnClicked(index->phone(index)).setPreferredWidth(50) 
    };
    
    component = new SgPanel(new SgBorderLayout.V(
      null,
      new SgScrollPane(
        table = new SgTable<>(columns).setRowHeight(25)
      ),
      new SgPanel(new SgFlowLayout(
        new SgButton("clear records", this::clearRecords),
        new SgButton("clear selection", this::clearSelection),
        new SgButton("add record", this::addRecord),
        new SgButton("reset records", this::resetRecords),
        new SgButton("delete record", this::deleteRecord),
        new SgButton("changeRecord", this::changeRecord)
      ))
    ));
    selectionModel = table.getSelectionModel();
    selectionModel.setMultiple(true);
    selectionModel.setSelectionHandler(this::selection);
    
    table.setRows(list.stream());    
  }
  
  private void changeRecord(SgButton b) {
    int index = selectionModel.getSelection().getMinIndex();
    if (index < 0) return;
    table.updateRow(index,  new Addr("山本", "999999"));
  }

  private void deleteRecord(SgButton b) {
    Selection selection = selectionModel.getSelection();
    if (selection.isEmpty()) return;
    table.removeRow(selection.getMinIndex());
  }
  
  /** 選択イベントの表示 */
  private void selection(SgListSelectionModel.Selection e) {
    System.out.println("selection " + e);
  }
  
  private void clearRecords(SgButton b) {
    table.clearRows();
  }
  
  private void clearSelection(SgButton b) {
    selectionModel.clear();
  }

  private void addRecord(SgButton b) {
    int index =  table.addRow(new Addr("田中", "000000"));
    selectionModel.select(index);
    table.scrollToVisible(index,  0);
  }
  
  private void resetRecords(SgButton b) {
    List<Addr>list = new ArrayList<>();
    for (int i = 0; i < 100; i++) {
      list.add(new Addr("上尾", "88889999"));
      list.add(new Addr("浦和", "00998899"));
    }
    table.setRows(list.stream());  
    table.scrollToVisible(0,  0);
  }
  
  private void phone(Integer index) {
    SgDialogs.showMessage(table, "Phone",  table.getRow(index).phone);
  }

  static class Addr {
    String name;
    String phone;
    Addr(String name, String phone) {
      this.name = name;
      this.phone = phone;
    }
  }
}
