package com.cm55.sg.sample;

import java.awt.*;

import com.cm55.sg.*;
import com.cm55.sg.SgWindow.*;

public class ButtonsTab {

  SgComponent<?>component;

  
  public ButtonsTab() {
    SgButton button;
    SgCheckBox checkBox;
    SgToggleButton toggleButton;
    
    SgRadioButtons radioButtons;
    component = new SgGroupBox("ボタングループ", new SgFlowLayout(
      button = new SgButton("サンプルのボタンです", this::message),
      new SgButton("B", this::dialog),
      new SgButton("C", this::font),
      checkBox = new SgCheckBox("チェックボックス", e->  {
        System.out.println("check " + e.getValue());
      }),
      new SgButton("チェックボックスON", e-> {
        checkBox.setValue(true);
      }),
      toggleButton = new SgToggleButton("トグル", e->  {
        System.out.println("toggle " + e.getValue());
      }),
      new SgButton("トグルON", e-> {
        toggleButton.setValue(true);
      }) ,
      
      radioButtons = new SgRadioButtons(
          SgRadioButtons.Type.TOGGLE_VERTICAL,
          this::radios, "A", "B", "C"),
      
      new SgButton("select B", e-> radioButtons.select(1) )
    ));
    
    button.w().setFont(new Font("メイリオ", 0, 30));
  }
  
  private void radios(Integer index) {
    System.out.println("radio " + index);
  }
  
  private void message(SgButton button) {
    SgDialogs.showMessage(button, "ボタンクリック",  "button " + button.getText());
  }

  SgDialog dialog = new SgDialog().setModal().setLayout(new SgBorderLayout.V(
      new SgButton("top"), 
      new SgButton("center"), 
      new SgButton("bottom")
  ))
  .setCloseHandler(()->CloseAction.EXIT)
  .setSize(400, 400);
  
  private void dialog(SgButton button) {


    dialog.setVisible(true);    
  }
  
  private void font(SgButton b) {
   
  }

}
