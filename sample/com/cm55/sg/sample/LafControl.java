package com.cm55.sg.sample;

import static com.cm55.sg.SgLaf.*;

import java.awt.*;
import java.util.stream.*;

import javax.swing.*;
import javax.swing.plaf.nimbus.*;

import com.cm55.sg.*;

public class LafControl {

  private final static SgLaf[]AVAIL_LAFS = {
      NIMBUS,
      METAL,
      WINDOWS,
      GTK,
      MOTIF,
      NAPKIN,
      WEB,
      QUAQUA
  };
  
  private final static AbstractMenuItem<?>[]MENU_ITEMS = IntStream.range(0, AVAIL_LAFS.length).mapToObj(i -> {
    SgLaf laf = AVAIL_LAFS[i];
    return new SgMenuItem(laf.name, e->LafControl.change(i));    
  }).collect(Collectors.toList()).toArray(new SgMenuItem[0]);
      
  private final static SgMenu MENU = new SgMenu("", MENU_ITEMS);

  public static void initialize() { 
    System.setProperty("awt.useSystemAAFontSettings","on");
    System.setProperty("swing.aatext", "true");
    
    try {
      Font font = new Font("メイリオ", 0, 16);
      UIManager.setLookAndFeel(new NimbusLookAndFeel() {

        @Override
        public UIDefaults getDefaults() {
         UIDefaults ret = super.getDefaults();
         ret.put("defaultFont", font);
         return ret;
        }

       });
      MENU.setText(AVAIL_LAFS[0].name);
    } catch (Exception ex) {      
    }
  }
  
  public static SgMenu getMenu() {
    return MENU;
  }
  
  private static void change(int index) {
    
    SgLaf laf = AVAIL_LAFS[index];
    try {
      UIManager.setLookAndFeel(laf.clazz);
      MENU.setText(laf.name);
    } catch (Exception ex) {
      return;
    }
    SgFrame.getFrames().forEach(frame->
      SwingUtilities.updateComponentTreeUI(frame.w()));
    
  }

}
