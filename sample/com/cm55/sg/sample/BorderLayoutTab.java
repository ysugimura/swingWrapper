package com.cm55.sg.sample;

import com.cm55.sg.*;

public class BorderLayoutTab {

  public SgComponent<?> component;
  
  public BorderLayoutTab() {
    
    SgPanel leftPanel = new SgPanel(new SgButtonLayout.V(2, 
      new SgButton("one"),
      new SgButton("two"),
      new SgButton("three")
    ));
    
    
    /*
    JButton button1 = new JButton("Google");
    JButton button2 = new JButton("Yahoo!");
    button2.setFont(new Font("Arial", Font.PLAIN, 30));
    JButton button3 = new JButton("MSN");

    JPanel p = new JPanel();
    p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));

    p.add(button1);
    p.add(button2);
    p.add(button3);
    */
    
    component = new SgPanel(new SgBorderLayout(
      new SgGroupBox("A", new SgFlowLayout(new SgLabel("top"))),
      new SgGroupBox("B", new SgFlowLayout(new SgLabel("bottom"))),
      //SgComponent.wrap(p),
      leftPanel,
      new SgLabel("center"),
      new SgLabel("right")
    ));
  }

}
