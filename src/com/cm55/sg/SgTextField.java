package com.cm55.sg;

import java.awt.event.*;
import java.util.*;
import java.util.function.*;

import javax.swing.*;

/**
 * {@link JTextField}のラッパ
 * @author ysugimura
 */
public class SgTextField extends SgTextComponent<SgTextField> {

  /** Swingコンポーネント */
  JTextField textField;
  
  /** Enter押下時のハンドラ */
  Optional<Consumer<SgTextField>>enterHandler = Optional.empty();
  
  /**
   * 通常テキスト用フィールドを作成する
   */
  public SgTextField() {
    this(false);
  }
  
  /** 
   * 作成する
   * @param password true:パスワード用、false:通常テキスト用
   */
  public SgTextField(boolean password) {
    if (password) 
      textField = new JPasswordField();
    else 
      textField = new JTextField();
    
    textField.addKeyListener(new KeyAdapter() {
      @Override
      public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == 10) {
          enterHandler.ifPresent(h->h.accept(SgTextField.this));
        }      
      }      
    });    
  }

  /** 
   * Enterハンドラを設定する。
   * Enterで何らかのアクションを行う場合に使用する。
   * @param handler Enterが押された時に呼び出されるハンドラ
   * @return 本オブジェクト
   */
  public SgTextField setEnterHandler(Consumer<SgTextField>handler) {
    enterHandler = Optional.of(handler);
    return this;
  }
  
  /** Swingの{@link JTextField}を取得する */
  @Override
  public JTextField w() {
    return textField;
  }
  
}
