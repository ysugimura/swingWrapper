package com.cm55.sg;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.function.*;

import javax.swing.*;

public abstract class SgWindow<T extends SgWindow<T>> extends SgContainer<T> {

  /**
   * ウインドウクローズ時の動作定義
   * <p>
   * これは{@link WindowConstants}の再定義
   * </p>
   */
  public static enum CloseAction {
    /** 何もしない */
    NONE,
    /** 隠す */
    HIDE,
    /** 廃棄する */
    DISPOSE,
    /** アプリをexitする */
    EXIT;
  }

  /** 
   * ウインドウクローズボタン押下時のアクションを指定する
   * デフォルトは{@link CloseAction.HIDE}
   * @param action {@link CloseActio}の中で指定するアクション
   * @return 本オブジェクト
   */
  @SuppressWarnings("unchecked")
  public T setCloseAction(CloseAction action) {
    closeAction = action;
    return (T) this;
  }

  /**
   * ウインドウクローズハンドラを設定する。
   * 例えば、ウインドウクローズボタンが押されたとき、本当にクローズして
   * 良いかを確認すべき場合は、このハンドラを設定してコールバックさせる。
   * その返り値として{@link CloseAction}の任意の値を返す。 
   * @param handler {@link CloseAction}値を返すハンドラ
   * @return 本オブジェクト
   */
  @SuppressWarnings("unchecked")
  public T setCloseHandler(Supplier<CloseAction> handler) {
    setCloseAction(CloseAction.NONE);
    closeHandler = Optional.of(handler);
    return (T) this;
  }

  /** 位置・サイズを取得する */
  public Rectangle getBounds() {
    return w().getBounds();
  }

  /**
   * 位置とサイズを指定する
   */
  @SuppressWarnings("unchecked")
  public T setBounds(int x, int y, int w, int h) {
    w().setBounds(x, y, w, h);
    return (T) this;
  }

  /**
   * サイズを設定する
   * 
   * @param width
   *          幅
   * @param height
   *          高さ
   * @return 本オブジェクト
   */
  @SuppressWarnings("unchecked")
  public T setSize(int width, int height) {
    w().setSize(width, height);
    return (T) this;
  }

  /**
   * 表示のON/OFF
   * 
   * @param value
   *          true:表示する。false:閉じる
   * @return 本オブジェクト
   */
  @SuppressWarnings("unchecked")
  public T setVisible(boolean value) {
    w().setVisible(value);
    return (T) this;
  }

  @Override
  public abstract Window w();

  /* ウインドウクローズのハンドリング ===================================== */


  CloseAction closeAction = CloseAction.HIDE;
  Runnable disposedHandler;
  Optional<Supplier<CloseAction>> closeHandler = Optional.empty();

  protected void windowSetup(Window window, Runnable disposedHandler) {
    if (window instanceof JDialog)
      ((JDialog) window).setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    else if (window instanceof JFrame)
      ((JFrame) window).setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    else
      throw new RuntimeException("Not supported");

    window.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        CloseAction a = closeHandler.map(h -> h.get()).orElse(closeAction);
        switch (a) {
        default:
          break;
        case HIDE:
          window.setVisible(false);
          break;
        case DISPOSE:
          window.dispose();
          break;
        case EXIT:
          System.exit(0);
        }
      }
    });
  }
}
