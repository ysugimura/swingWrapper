package com.cm55.sg;

import javax.swing.border.*;

/**
 * Swingの{@link Border}を表現する
 * @author ysugimura
 */
public class SgBorder {

  /**
   * 空のボーダー、つまりパディングを作成する
   * @param top
   */
  public static final SgBorder createPadding(int padding) {
    return new SgBorder(new EmptyBorder(padding, padding, padding, padding));
  }
  
  /**
   * タイトル付グループボックスを作成する
   */
  /* SgGroupBoxがあれば要らない気がする。
  public static final SgBorder createGroupBox(String title, int padding) {
    return new SgBorder(new TitledBorder(
        new EmptyBorder(padding, padding, padding, padding), title));
  }
  */
  
  final Border border;
  
  SgBorder(Border border) {
    this.border = border;
  }

}
