package com.cm55.sg;

import java.awt.*;
import java.util.*;

public class SgFlowLayout extends SgLayout {

  private java.util.List<SgComponent<?>>components = new ArrayList<>();
  private Container container;
  
  public SgFlowLayout() {    
  }
  
  public SgFlowLayout(SgComponent<?>...c) {
    add(c);
  }
  
  public SgFlowLayout add(SgComponent<?>...c) {
    components.addAll(Arrays.asList(c));
    return this;
  }

  @Override
  public void applyTo(Container container) {
    this.container = container;
    container.setLayout(new FlowLayout());
    components.stream().forEach(c->container.add(c.w()));
  }
}
