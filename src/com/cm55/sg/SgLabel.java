package com.cm55.sg;

import javax.swing.*;

/**
 * {@link JLabel}のラッパ
 * @author ysugimura
 */
public class SgLabel extends SgComponent<SgLabel> {

  protected JLabel label;
  
  /**
   * ラベル文字列を指定して作成する
   * @param text ラベル文字列
   */
  public SgLabel(String text) {
    label = new JLabel(text);   
 
  }
  
  /** ラベル文字列の水平アラインメントを指定する */
  public SgLabel setHorAlign(SgHorAlign align) {
    label.setHorizontalAlignment(align.value);
    return this;
  }
  
  /** 元の{@link JLabel}を取得する */
  @Override
  public JLabel w() {
    return label;
  }
}
