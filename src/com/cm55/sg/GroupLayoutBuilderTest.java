package com.cm55.sg;

import javax.swing.*;
import javax.swing.GroupLayout.*;

public class GroupLayoutBuilderTest {

  public static void main(String[]args) {
    JFrame frame = new JFrame();
    new GroupLayoutBuilder()
        .addRow(Alignment.BASELINE, new JLabel("ユーザ名を入力"), null, new JTextField())
        .addRow(Alignment.BASELINE, new JLabel("パスワード"), new JButton("詳細"), new JTextField(), new JLabel("test"))
        .setColAlign(Alignment.TRAILING, Alignment.LEADING)
        .build(frame.getContentPane());
    frame.setBounds(200, 200, 400, 400);   
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setVisible(true);
  }
}
