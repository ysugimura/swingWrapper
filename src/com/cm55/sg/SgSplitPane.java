package com.cm55.sg;

import javax.swing.*;

public abstract class SgSplitPane extends SgComponent<SgSplitPane> {

  JSplitPane splitPane;
  
  public SgSplitPane(int orientation, SgComponent<?> a, SgComponent<?> b) {
    splitPane = new JSplitPane(orientation, true, a.w(), b.w());
  }

  public int getDividerLocation() {
    return splitPane.getDividerLocation();
  }
  
  public SgSplitPane setDividerLocation(int value) {
    splitPane.setDividerLocation(value);
    return this;
  }
  
  @Override
  public JComponent w() {
    return splitPane;
  }
  
  public static class V extends SgSplitPane {
    public V(SgComponent<?>a, SgComponent<?>b) {
      super(JSplitPane.VERTICAL_SPLIT, a, b);
    }
  }
  
  public static class H extends SgSplitPane {
    public H(SgComponent<?>a, SgComponent<?>b) {
      super(JSplitPane.HORIZONTAL_SPLIT, a, b);
    }
  }
}
