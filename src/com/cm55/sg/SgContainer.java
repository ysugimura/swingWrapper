package com.cm55.sg;

import java.awt.*;

import javax.swing.*;

/**
 * これは、java.awt.Containerのラッパであることに注意。
 * この下位にSwingのJComponentがある。
 * 本来の意味としては逆である。以下であるべきだが、
 * <pre>
 * SgComponent
 *  +- SgContainer
 * </pre>
 * <p>
 * しかし、逆に以下になっている。
 * </p>
 * <pre>
 * SgContainer
 * +- SgComponent
 * </pre>
 * <p>
 * この理由としては、本来のSwingのクラス階層が、AWTとの互換性の
 * ために以下になっているからである。
 * </p>
 * <pre>
 * Component
 * +- Container
 *    +- JComponent
 * </pre>
 * <p>
 * 本ライブラリでは、Componentは無視するが、ContainerとJComponentは必要なために
 * 以下の階層になっている。このために本来の意味とは逆になっている。
 * </p>
 * <pre>
 * +- Container      SgContainer
 *    +- JComponent  SgComponent
 * </pre>
 * 
 * @author ysugimura
 *
 * @param <T> 下位クラスの型
 */
public abstract class SgContainer<T extends SgContainer<T>>  {
 
  /**
   * このコンテナが所属するSwingの{@link Window}オブジェクトを取得する。
   * @return {@link Window}オブジェクト
   */
  public Window getWindow() {
    return SwingUtilities.getWindowAncestor((Container)w());
  }
  
  public abstract Container w();
}
