package com.cm55.sg;

import java.awt.*;
import java.util.function.*;

import javax.swing.*;
import javax.swing.table.*;

/**
 * ボタン列のテンプレート
 * <p>
 * テーブルの中にボタンを入れる場合のテンプレート。
 * テーブル中には通常のボタンは格納できない。
 * </p>
 * @author ysugimura
 *
 * @param <R>
 */
public abstract class AbstractButtonColumn<R, T extends AbstractButtonColumn<R, T>> extends SgColumn<R, T> {

  /** 唯一のボタンレンダラ */
  private static final Renderer renderer = new Renderer();
  
  /** 唯一のボタンエディタ */
  private static final Editor editor = new Editor();

  /** クリック時のコールバック。引数として行インデックスが与えられる。nullの時には何もしない */
  protected Consumer<Integer>clicked;
  
  /** 列タイトルを指定する */
  public AbstractButtonColumn(String title) {
    super(title, String.class);
    this.column.setCellRenderer(renderer);
    this.column.setCellEditor(editor);
  }

  /** クリック時コールバックを設定する */
  @SuppressWarnings("unchecked")
  public T setOnClicked(Consumer<Integer>clicked) {
    this.clicked = clicked;
    return (T)this;
  }

  /** 
   * ボタン列コンテキスト。テーブル側には、これをセルデータとして返すこと
   * @author ysugimura
   */
  protected static abstract class AbstractContext {
    
    /** クリック時コールバック */
    protected final Consumer<Integer>clicked;
    
    protected AbstractContext(Consumer<Integer>clicked) {
      this.clicked = clicked;
    }

    /** ボタンクリック時にコールバックを行う */
    protected void clicked(int row) {
      if (clicked != null) clicked.accept(row);
    }

    /** ボタン状態の変更 */
    protected abstract void changeButtonState(JTable table, JButton button, boolean isSelected);
  }

  /** ボタンレンダラ */
  private static class Renderer implements TableCellRenderer {
    private final JButton button = new JButton();
    public Renderer() {
      button.setOpaque(true);
    }
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
        int row, int column) {
      AbstractContext context = (AbstractContext)value;
      context.changeButtonState(table, button, isSelected);
      return button;
    }
  }

  /** ボタンエディタ */
  private static class Editor extends DefaultCellEditor {
    private final JButton button;
    private int clickedRow = -1;
    private AbstractContext context;    
    
    Editor() {
      super(new JCheckBox());
      button = new JButton();
      button.setOpaque(true);
      button.addActionListener(e-> {
        try {
          fireEditingStopped();
        } catch (ArrayIndexOutOfBoundsException ex) {
          // 行数が0になったときに、この例外が発生する。原因不明
        }
      });
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
      context = (AbstractContext)value;
      context.changeButtonState(table, button, isSelected);
      clickedRow = row;
      return button;
    }
    
    @Override
    public Object getCellEditorValue() {
      if (clickedRow >= 0) {
        context.clicked(clickedRow);
      }
      clickedRow = -1;
      return context;
    }

    @Override
    public boolean stopCellEditing() {
      clickedRow = -1;
      return super.stopCellEditing();
    }
  }
}
