package com.cm55.sg;

import java.awt.event.*;
import java.util.*;

import javax.swing.*;

public class SgPopupMenu extends SgComponent<SgPopupMenu> {

  JPopupMenu popupMenu;
  
  public SgPopupMenu(SgMenuItem...items) {
    popupMenu = new JPopupMenu();
    Arrays.stream(items).forEach(i->popupMenu.add(i.menuItem));
  }

  @Override
  public JComponent w() {
    return popupMenu;
  }

  public void attachOn(SgComponent<?>component) {
    component.w().addMouseListener(new MouseAdapter() {
      public void mousePressed(MouseEvent e) {
        if (e.isPopupTrigger())
            doPop(e);
      }

      public void mouseReleased(MouseEvent e) {
        if (e.isPopupTrigger())
            doPop(e);
      }

      private void doPop(MouseEvent e) {
        popupMenu.show(e.getComponent(), e.getX(), e.getY());
      }
    });
  }
}
