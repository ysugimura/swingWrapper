package com.cm55.sg;

import java.util.function.*;

import javax.swing.*;

/**
 * {@link JButton}のラッパ
 * @author ysugimura
 */
public class SgButton extends SgAbstractButton<SgButton> {

  JButton button;
  
  /** ラベルテキストを指定する */
  public SgButton(String text) {
    this(text, null);
  }

  /** ラベルテキストとクリックハンドラを指定する */
  public SgButton(String text, Consumer<SgButton>handler) {
    button = new JButton(text);
    if (handler != null)
      button.addActionListener(e->handler.accept(this));
  }

  /** ラベルテキストを取得する */
  public String getText() {
    return button.getText();
  }
  
  /** {@link JButton}を取得する */
  @Override
  public JButton w() {
    return button;
  }
}
