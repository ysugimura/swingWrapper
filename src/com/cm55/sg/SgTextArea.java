package com.cm55.sg;

import javax.swing.*;

/**
 * {@link JTextArea}をラップしたもの。{@link JTextArea}と異なるデフォルト動作は以下。
 * <ul>
 * <li>ラインナップがONになっている。つまり、水平スクロールバーが出ずに、行が
 * 折り返される。
 * </ul>
 * @author ysugimura
 */
public class SgTextArea extends SgTextComponent<SgTextArea> {

  JTextArea textArea;
  
  public SgTextArea() {
    textArea = new JTextArea();
    textArea.setLineWrap(true);
  }

  /** ラインナップを設定する */
  public SgTextArea setLineWrap(boolean value) {
    textArea.setLineWrap(value);
    return this;
  }

  /** テキストを追加する。キャレット位置にかかわらず、常に最後になる */
  public SgTextArea append(String text) {
    textArea.append(text);
    return this;
  }
  
  /** Swingの{@link JScrollPane}を取得する */
  @Override
  public JTextArea w() {
    return textArea;
  }
}
