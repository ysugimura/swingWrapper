package com.cm55.sg;

import java.awt.event.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

import javax.swing.*;

/**
 * ラジオボタングループ
 * @author ysugimura
 */
public class SgRadioButtons extends SgComponent<SgRadioButtons> {

  public static enum Type {
    RADIO_HORIZONTAL(JRadioButton.class, false),
    RADIO_VERTICAL(JRadioButton.class, true),
    TOGGLE_HORIZONTAL(JToggleButton.class, false),
    TOGGLE_VERTICAL(JToggleButton.class, false);
    
    private final Class<? extends JToggleButton>clazz;
    private final boolean vertical;
    private Type(Class<? extends JToggleButton>clazz, boolean vertical) {
      this.clazz = clazz;
      this.vertical = vertical;
    }
  }
  
  JPanel panel;
  List<JToggleButton>buttons;
  int selection = -1;
  
  public SgRadioButtons(Type type, Consumer<Integer>handler, String...labels) {
    panel = new JPanel();
    
    if (!type.vertical) 
      panel.setLayout(new ButtonLayout.H());
    else 
      panel.setLayout(new ButtonLayout.V());
    
    buttons = Arrays.stream(labels)
        .map(l->create(type.clazz, l))
        .collect(Collectors.toList());
    
    ButtonGroup buttonGroup = new ButtonGroup();
    buttons.forEach(button-> {
      button.addActionListener(e->handling(e, handler));      
      buttonGroup.add(button);
      panel.add(button);      
    });    
  }
  
  private JToggleButton create(Class<? extends JToggleButton>clazz, String label) {
    JToggleButton button;
    try {
      button = clazz.getDeclaredConstructor().newInstance();
    } catch (Exception ex) {
      throw new RuntimeException("Internal Error");
    }
    button.setText(label);
    return button;
  }

  /** イベントハンドリング */
  private void handling(ActionEvent e, Consumer<Integer>handler) {
    int newSelection = buttons.indexOf(e.getSource());
    if (selection == newSelection) return;
    selection = newSelection;

    handler.accept(selection);
  }
  
  /** 選択を取得 */
  public int getSelection() {
    return IntStream.range(0,  buttons.size())
      .filter(i->buttons.get(i).isSelected())
      .findAny().orElse(-1);
  }

  /** 
   * 選択を設定。イベントは発行されない
   * 負値を指定することにより、全被選択にすることもできる。
   * @param index
   */
  public void select(int index) {
    if (index == selection) return;
       
    if (index < 0) {
      if (selection < 0) return;
      buttons.get(selection).setSelected(false);
      selection = index;
      return;
    }
    
    
    buttons.get(index).setSelected(true);
    selection = index;
  }
  
  
  @Override
  public JComponent w() {
    return panel;
  }

}
