package com.cm55.sg;

import javax.swing.*;

public class SgPanel extends SgComponent<SgPanel> {

  JPanel panel;
  
  public SgPanel(SgLayout layout) {
    panel = new JPanel();
    layout.applyTo(panel);
  }

  public JPanel w() {
    return panel;
  }  
}
