package com.cm55.sg;

import javax.swing.text.*;

public abstract class SgTextComponent<T extends SgTextComponent<T>> 
  extends SgComponent<T> { 


  /** 手入力での編集可不可を設定する */
  @SuppressWarnings("unchecked")
  public T setEditable(boolean value) {
    w().setEditable(value);
    return (T)this;
  }
  
  /** 既存の内容をクリアしてテキストを設定する */
  @SuppressWarnings("unchecked")
  public T setText(String text) {
    w().setText(text);
    return (T)this;
  }
  
  /** 現在のテキストを取得する */
  public String getText() {
    return w().getText();
  }
  
  /** 選択中テキストをクリップボードに転送する */
  @SuppressWarnings("unchecked")
  public T copy() {
    w().copy();
    return (T)this;
  }

  /** クリップボード内テキストを現在位置に挿入する */
  @SuppressWarnings("unchecked")
  public T paste() {
    w().paste();
    return (T)this;
  }
  
  @Override
  public abstract JTextComponent w();
}
