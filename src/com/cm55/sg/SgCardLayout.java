package com.cm55.sg;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.*;

public class SgCardLayout extends SgLayout {

  Container container;
  CardLayout layout;
  List<SgComponent<?>>components = new ArrayList<>();
  
  public SgCardLayout() {
  }
  
  public SgCardLayout(SgComponent<?>...c) {
    add(c);
  }
  
  public SgCardLayout add(SgComponent<?>...c) {
    components.addAll(Arrays.asList(c));
    return this;
  }

  public void show(int index) {
    layout.show(container, index + "");
  }
  
  @Override
  public void applyTo(Container container) {
    this.container = container;
    container.setLayout(layout = new CardLayout());
    
    IntStream.range(0, components.size()).forEach(i-> {
      SgComponent<?>c = components.get(i);
      container.add(c.w(), i + "");
    });
    
    components.stream().forEach(c->container.add(c.w()));    
  }
}
