package com.cm55.sg;

import java.awt.*;
import java.util.*;

/**
 * ボタンを縦並び・横並びにしたときに、すべての幅を最大に合わせるレイアウト
 * <p>
 * 例えばボタンを縦並びにしたときに、すべてのボタン幅を最大のものに合わせる
 * 機能がSwingには無い。
 * {@link SgButtonLayout}はそのような用途に用いる。最も考えられる状況としては、
 * 左あるいは右に縦並びにボタンを配置した場合に、それらのボタンをすべて
 * 同じサイズにしたいという場合だろう。
 * </p>
 * <p>
 * このレイアウトでは、縦並びのみならず、横並びの場合にもボタンサイズを統一
 * する。
 * </p>
 * 
 * @author ysugimura
 */
public abstract class SgButtonLayout extends SgLayout {

  final int gap;
  final SgContainer<?>[]containers;
  
  public SgButtonLayout(int gap, SgContainer<?>...containers) {
    this.gap = gap;
    this.containers = containers;
  }

  @Override
  public void applyTo(Container container) {  
    ButtonLayout layout;
    if (this instanceof H) 
      layout = new ButtonLayout.H(gap);
    else 
      layout = new ButtonLayout.V(gap);
    
    container.setLayout(layout);
    
    Arrays.stream(containers).forEach(c-> {
      container.add(c.w());
    });
  }
  
  public static class H extends SgButtonLayout {
    public H(int gap, SgContainer<?>...containers) {
      super(gap, containers);
    }
  }
  
  public static class V extends SgButtonLayout {
    public V(int gap, SgContainer<?>...containers) {
      super(gap, containers);
    }
  }
}
