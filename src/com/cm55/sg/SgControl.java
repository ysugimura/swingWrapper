package com.cm55.sg;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.*;

public class SgControl {

  /**
   * OSにインストールされている日本語フォントを得る
   * @return
   */
  public static List<Font>getHJapaneseFonts() {
    return Arrays.stream(GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts())
      .filter(f->f.canDisplay('あ'))
      .collect(Collectors.toList());
  }  
  
}
