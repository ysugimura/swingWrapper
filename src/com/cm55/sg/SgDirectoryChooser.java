package com.cm55.sg;

import java.nio.file.*;

import javax.swing.*;

/**
 * フォルダを選択するダイアログ
 * @author ysugimura
 */
public class SgDirectoryChooser {

  static JFileChooser chooser = new JFileChooser();

  public static Path choose(SgComponent<?>parent) {
    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    if (chooser.showOpenDialog(parent.w()) == JFileChooser.APPROVE_OPTION) {
      return chooser.getSelectedFile().toPath();
    }
    return null;
  }

}
