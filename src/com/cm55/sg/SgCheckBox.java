package com.cm55.sg;

import java.util.function.*;

import javax.swing.*;

/**
 * {@link JCheckBox}ラッパ
 * @author ysugimura
 */
public class SgCheckBox extends SgAbstractButton<SgCheckBox> {

  JCheckBox checkBox;

  public SgCheckBox(String label) {
    this(label, null);
  }
  
  public SgCheckBox(String label, Consumer<SgCheckBox>handler) {
    checkBox = new JCheckBox(label);
    if (handler != null) checkBox.addActionListener(e->handler.accept(this));
  }

  /**
   * 値を設定する。イベントは発行されない
   * @param value
   */
  public void setValue(boolean value) {
    checkBox.setSelected(value);
  }
  
  /**
   * 値を取得する
   */
  public boolean getValue() {
    return checkBox.isSelected();
  }

  @Override
  public JCheckBox w() {
    return checkBox;
  }
}
