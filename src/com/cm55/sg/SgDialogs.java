package com.cm55.sg;

import javax.swing.*;

/**
 * メッセージ表示、YES/NOの選択、OK/Cancelの選択等のダイアログを表示する。
 * <p>
 * 親コンテナはnullとすることもできるが、指定すると、その親の上にダイアログ
 * が表示される。
 * </p>
 * @author ysugimura
 */
public class SgDialogs {

  /**
   * メッセージを表示する
   * @param parent 親コンポーネント
   * @param title タイトル
   * @param message メッセージ
   */
  public static void showMessage(SgContainer<?> parent, String title, String message) {
    JOptionPane.showMessageDialog(
      parent == null? null:parent.w(), 
      message, 
      title,
      JOptionPane.INFORMATION_MESSAGE); 
  }

  /**
   * 親コンポーネント、タイトル、メッセージを指定し、Yes/Noダイアログを表示する。Yesの場合はtrueが変える。
   * @param parent 親コンポーネント
   * @param title タイトル
   * @param message メッセージ
   * @return
   */
  public static boolean confirmYes(SgContainer<?> parent, String title, String message) {
    int option = JOptionPane.showConfirmDialog(parent.w(), message, title, 
      JOptionPane.OK_CANCEL_OPTION,
      JOptionPane.QUESTION_MESSAGE
    );
    return option == JOptionPane.YES_OPTION;
  }

  /**
   * 親コンポーネント、タイトル、メッセージを指定し、Yes/Noダイアログを表示する。Yesの場合はtrueが変える。
   * @param parent 親コンポーネント
   * @param title タイトル
   * @param message メッセージ
   * @return
   */
  public static boolean confirmOk(SgContainer<?> parent, String title, String message) {
    int option = JOptionPane.showConfirmDialog(parent.w(), message, title, 
      JOptionPane.OK_CANCEL_OPTION,
      JOptionPane.QUESTION_MESSAGE
    );
    return option == JOptionPane.YES_OPTION;
  }

}
