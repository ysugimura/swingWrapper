package com.cm55.sg;

import java.util.*;

import javax.swing.*;

public class SgMenu extends AbstractMenuItem<SgMenu> {

  protected JMenu menu;

  /** 初期ラベル、メニューアイテムを指定する */
  public SgMenu(String label, AbstractMenuItem<?>...items) {
    menu = new JMenu(label);
    Arrays.stream(items).forEach(i->menu.add(i.w()));
  }
  
  /** メニューのラベルを変更する */
  public SgMenu setText(String text) {
    menu.setText(text);
    return this;
  }

  @Override
  public JMenu w() {
    return menu;
  }
}
