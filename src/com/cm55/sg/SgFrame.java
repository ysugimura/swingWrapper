package com.cm55.sg;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;
import java.util.stream.*;

import javax.swing.*;

public class SgFrame extends SgWindow<SgFrame> {

  JFrame frame;
  
  static List<SgFrame>frames = new ArrayList<>();
  
  public SgFrame(String title) {
    frame = new JFrame(title);  

    // 生きているフレームリストに追加する
    frames.add(this);
    
    // ウインドウクローズ時、破棄される場合はフレームリストから除去
    frame.addWindowListener(new WindowAdapter() {
      public void windowClosed(WindowEvent e) {
        if (frame.getDefaultCloseOperation() == JFrame.DISPOSE_ON_CLOSE)
          frames.remove(SgFrame.this);       
      }
    });    
    windowSetup(frame, ()->frames.remove(this));
  }
  
  public SgFrame(String title, SgLayout layout) {
    frame = new JFrame(title);   
    setLayout(layout);
  }
  
  public SgFrame setVisible(boolean value) {
    if (value) {
      frame.setLocationRelativeTo(null);
    }
    super.setVisible(value);
    return this;
  }
  
  public SgFrame setLayout(SgLayout layout) {
    layout.applyTo(frame);
    return this;
  }
  
  public SgFrame setLayout(LayoutManager layout) {
    frame.setLayout(layout);
    return this;
  }
  
  public SgFrame add(SgComponent<?> c, Object constrains) {
    frame.add(c.w(), constrains);
    return this;
  }
  
  public SgFrame setMenuBar(SgMenuBar menuBar) {
    frame.setJMenuBar(menuBar.w());

    return this;
  }
  
  @Override
  public JFrame w() {
    return frame;
  }  
  
  /** 現在生きている全{@link SgFrame}を取得する。これにはクローズされているものも含む */
  public static Stream<SgFrame>getFrames() {
    return frames.stream();
  }
}
