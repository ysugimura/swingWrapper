package com.cm55.sg;

import java.util.function.*;

import javax.swing.*;

/**
 * {@link JToggleButton}ラッパ
 * @author ysugimura
 */
public class SgToggleButton extends SgAbstractButton<SgToggleButton> {

  JToggleButton ToggleButton;

  public SgToggleButton(String label) {
    this(label, null);
  }
  
  public SgToggleButton(String label, Consumer<SgToggleButton>handler) {
    ToggleButton = new JToggleButton(label);
    if (handler != null) ToggleButton.addActionListener(e->handler.accept(this));
  }

  /**
   * 値を設定する。イベントは発行されない
   * @param value
   */
  public void setValue(boolean value) {
    ToggleButton.setSelected(value);
  }
  
  /**
   * 値を取得する
   */
  public boolean getValue() {
    return ToggleButton.isSelected();
  }

  @Override
  public JToggleButton w() {
    return ToggleButton;
  }
}
