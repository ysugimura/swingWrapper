package com.cm55.sg;

import javax.swing.*;

public class SgScrollPane extends SgComponent<SgScrollPane> {

  JScrollPane scrollPane;
  
  public SgScrollPane(SgComponent<?>content) {
    scrollPane = new JScrollPane(content.w());
  }

  @Override
  public JComponent w() {
    return scrollPane;
  }

}
