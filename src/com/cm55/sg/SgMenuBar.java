package com.cm55.sg;

import java.util.*;

import javax.swing.*;

/**
 * メニューバー
 * @author ysugimura
 */
public class SgMenuBar extends SgComponent<SgMenuBar> {

  protected final JMenuBar menuBar;
  
  public SgMenuBar(SgMenu...menus) {
    menuBar = new JMenuBar();
    Arrays.stream(menus).forEach(menu->menuBar.add(menu.menu));
  }

  @Override
  public JMenuBar w() {
    return menuBar;
  }

}
