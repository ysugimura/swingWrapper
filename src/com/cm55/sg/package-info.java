/**
 * Swingラッパ
 * <p>
 * Swingをラップして使いやすくしたライブラリ
 * </p>
 * 
 * <h2>Swingのクラス階層とラッパライブラリクラス階層</h2>
 * <p>
 * Swingを構成するクラスの階層は、それ以前のAWTのクラス階層に混ぜられているので
 * わかりにくものになっている。
 * </p>
 * <pre>
 * Object
 * +- Component
 *    +- Container
 *       +- JComponent
 *       |  +- JPanel
 *       |  +- MenuBar
 *       +- Window
 *          +- Frame
 *          | +- JFrame
 *          +- Dialog
 *             +- JDialog   
 * </pre>
 * <p>
 * つまり、Swingの機能はContainer以下しか使用していない。本ライブラリでは、
 * Containerに対応するクラスを{@link SgContainer}、JComponentに対応する
 * 機能を{@link SgComponent}とし、{@link Component}は考慮しない。
 * </p>
 * <p>
 * さらにウインドウ関連のクラスにも無視したものがある。
 * </p>
 * <p>
 * この上で対応するクラスを以下に示す。
 * </p>
 * <pre>
 * Container        SgContainer
 * +- JComponent    SgComponent
 * |  +- JPanel     SgPanel
 * |  +- MenuBar    SgMenuBar
 * +- Window        SgWindow
 *    +- Frame    
 *    |  +- JFrame  SgFrame
 *    +- Dialog
 *       +- JDialog SgDialog
 * </pre>
 * 
 * @author ysugimura
 */
package com.cm55.sg;