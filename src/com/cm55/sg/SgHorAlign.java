package com.cm55.sg;

import javax.swing.*;

public enum SgHorAlign {
  LEFT(SwingConstants.LEFT),
  CENTER(SwingConstants.CENTER),
  RIGHT(SwingConstants.RIGHT);
  
  public final int value;
  private SgHorAlign(int value) {
    this.value = value;
  }
}
