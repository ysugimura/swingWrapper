package com.cm55.sg;

import java.util.*;

import javax.swing.*;

/**
 * ダイアログ
 * <p>
 * 
 * </p>
 * @see <a href="http://www.ne.jp/asahi/hishidama/home/tech/java/swing/JDialog.html">
 * JDialog（Swing）</a>
 * @author ysugimura
 */
public class SgDialog extends SgWindow<SgDialog> {

  /** ダイアログ */
  final JDialog dialog;

  
  /** オーナーコンテナ */
  final Optional<SgContainer<?>> ownerContainer;
  
  /**
   * オーナー無しのダイアログを作成する
   */
  public SgDialog() {
    this(Optional.empty());
  }

  /**
   * オーナーありのダイアログを作成する
   * @param owner オーナー
   */
  public SgDialog(SgContainer<?>owner) {
    this(Optional.of(owner));
  }
  
  /**
   * ダイアログを作成する。
   * 注意：オーナーだけは後から指定することができない。
   * コンストラクタで指定する必要がある。
   * @param owner オーナー
   */
  private SgDialog(Optional<SgContainer<?>> owner) {
    ownerContainer = owner;
    dialog = owner
        .map(o->new JDialog(o.getWindow()))
        .orElseGet(()->new JDialog());
    windowSetup(dialog, ()->{});
  }

  public SgDialog close() {
    dialog.setVisible(false);
    return this;
  }
  
  public SgDialog dispose() {
    dialog.dispose();
    return this;
  }
  
  /**
   * 表示前にセンタリングを行う
   */
  @Override
  public SgDialog setVisible(boolean value) {
    dialog.setLocationRelativeTo(
      ownerContainer.map(o->o.w()).orElse(null)
    );
    super.setVisible(value);
    return this;
  }

  /**
   * タイトルを設定する
   */
  public SgDialog setTitle(String title) {
    dialog.setTitle(title);
    return this;
  }
  
  /**
   * モーダルダイアログにする
   * @return 本オブジェクト
   */
  public SgDialog setModal() {
    dialog.setModal(true);
    return this;
  }
  
  /**
   * コンテンツペインに対するレイアウトを設定する
   * @param layout レイアウト
   * @return 本オブジェクト
   */
  public SgDialog setLayout(SgLayout layout) {
    layout.applyTo(dialog.getContentPane());
    return this;
  }
  
  /**
   * コンポーネントを設定する
   * @param component コンポーネント
   * @return 本オブジェクト
   */
  public SgDialog setComponent(SgComponent<?>component) {
    dialog.getContentPane().add(component.w());
    return this;
  }
  
  /**
   * Swingの{@link JDialog}を返す
   */
  @Override
  public JDialog w() {
    return dialog;
  }
}
