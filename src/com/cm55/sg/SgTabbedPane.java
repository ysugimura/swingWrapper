package com.cm55.sg;

import java.util.*;
import java.util.function.*;

import javax.swing.*;
import javax.swing.event.*;

/**
 * Swingの{@link JTabbedPane}ラッパ
 * @author ysugimura
 */
public class SgTabbedPane extends SgComponent<SgTabbedPane> {

  JTabbedPane tabbedPane;
  private int currentSelection = -1;
  Optional<BiConsumer<Integer, Integer>>selectionHandler = Optional.empty();

  
  /** 作成する */
  public SgTabbedPane() {
    tabbedPane = new JTabbedPane();
    tabbedPane.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {        
        int previous = currentSelection;
        currentSelection = tabbedPane.getSelectedIndex();        
        selectionHandler.ifPresent(h->h.accept(previous, currentSelection));
      }
    });
  }
  
  /** 
   * 名前付でタブを追加する
   * @param name タブの名称
   * @param c タブコンポーネント
   * @return 本オブジェクト
   */
  public SgTabbedPane addTab(String name, SgComponent<?>c) {
    tabbedPane.addTab(name, c.w());
    return this;
  }
  
  /** 
   * 選択ハンドラを設定する
   * <p>
   * タブが選択された時に、以前のインデックスと新たなインデックスを通知する。
   * ただし、プログラムから{@link #select(int)}が行われた時には起動されない。
   * </p>
   * @param selectionHandler 選択ハンドラ
   * @return
   */
  public SgTabbedPane setSelectionHandler(BiConsumer<Integer, Integer>selectionHandler) {
    this.selectionHandler = Optional.of(selectionHandler);
    return this;
  }
  
  /** 
   * 現在の選択インデックスを返す
   * @return 現在の選択インデックス
   */
  public int getSelection() {
    return tabbedPane.getSelectedIndex();
  }

  /**
   * 現在のタブを切り替える。タブが変わった場合はイベントが発行される。
   * @param index 切り替え先のタブインデックス
   * @return 本オブジェクト
   */
  public SgTabbedPane select(int index) {
    if (currentSelection == index) return this;
    tabbedPane.setSelectedIndex(index);
    return this;
  }

  /** Swingの{@link JTabbedPane}を返す */
  public JTabbedPane w() {
    return tabbedPane;
  }
}
