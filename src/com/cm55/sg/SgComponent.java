package com.cm55.sg;

import java.awt.*;

import javax.swing.*;

public abstract class SgComponent<T extends SgComponent<T>> extends SgContainer<T> {
    
  /** 有効無効設定 */
  @SuppressWarnings("unchecked")
  public T setEnabled(boolean value) {
    w().setEnabled(value);
    return (T)this;
  }

  /** 推奨サイズを指定する */
  @SuppressWarnings("unchecked")
  public T setPreferredSize(Dimension dimension) {
    w().setPreferredSize(dimension);
    return (T)this;
  }

    
  public abstract JComponent w();
  
  
  /** ラッパ ================================================================*/
  
  public static class Wrapper extends SgComponent<Wrapper> {
    JComponent component;
    public Wrapper(JComponent component) {
      this.component = component;
    }
    @Override
    public JComponent w() {
      return component;
    }
  }

  @SuppressWarnings("unchecked")
  public T setBorder(SgBorder border) {
    w().setBorder(border.border);
    return (T)this;
  }
  
  public static Wrapper wrap(JComponent component) {
    return new Wrapper(component);
  }
}
