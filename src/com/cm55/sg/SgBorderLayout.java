package com.cm55.sg;

import java.awt.*;

/**
 * ボーダーレイアウト
 * <p>
 * 与えられた全体領域について、まず上と下に必要な領域が与えられ、残りの領域の
 * 左と右に必要な領域が与えられ、残りが中央領域に与えられる。
 * </p>
 * @author ysugimura
 */
public class SgBorderLayout extends SgLayout {

  /** ボーダーレイアウトの位置を表すオブジェクト */
  private static final String[]POSITIONS = new String[] { 
    BorderLayout.NORTH,
    BorderLayout.SOUTH,
    BorderLayout.WEST,
    BorderLayout.CENTER,
    BorderLayout.EAST
  };

  /** 
   * レイアウトに格納されるコンポーネント。
   * nullがあっても良い。その場合は無視される。
   */
  SgComponent<?>[]components;
  
  /**
   * 上下左中央右の順序で中身のコンポーネントを指定する。
   * @param top 上
   * @param bottom 下
   * @param left 左
   * @param right 中央
   * @param center 
   */
  public SgBorderLayout(
      SgComponent<?>top, SgComponent<?>bottom, 
      SgComponent<?>left, SgComponent<?>center, SgComponent<?>right) {
    components = new SgComponent<?>[] {
      top, bottom, left, center, right
    };    
  }

  /**
   * このレイアウトをコンテナに適用する
   */
  @Override
  public void applyTo(Container container) {
    container.setLayout(new BorderLayout());      
    for (int i = 0; i < POSITIONS.length; i++) {
      if (components[i] != null) container.add(components[i].w(), POSITIONS[i]);
    }
  }

  /** 
   * 水平方向のみのボーダーレイアウト
   * つまり、左、中央、右の順
   * @author ysugimura
   */
  public static class H extends SgBorderLayout {
    public H(SgComponent<?>left, SgComponent<?>center, SgComponent<?>right) {
      super(null, null, left, center, right);
    }
  }

  /**
   * 垂直方向のみのボーダーレイアウト
   * 上、中央、下の順
   * @author ysugimura
   *
   */
  public static class V extends SgBorderLayout {
    public V(SgComponent<?>top, SgComponent<?>center, SgComponent<?>bottom) {
      super(top, bottom, null, center, null);
    }
  }


  
}
