package com.cm55.sg;

import java.util.*;
import java.util.stream.*;

import javax.swing.event.*;
import javax.swing.table.*;

/**
 * 行単位でデータ管理する{@link TableModel}
 * <p>
 * もともとの{@link JTable}の設計としてはスプレッドシートのようにセル単位で
 * データを出し入れすることを前提にしているのだが、これは不要。主にデータベース
 * を扱う場合は行指向で十分である。
 * </p>
 * <p>
 * ここでは、行単位で追加、更新、削除をすることに注力している。
 * </p>
 * @author ysugimura
 *
 * @param <T>
 */
public class RowTableModel<T> implements TableModel {

  SgColumn<T, ?>[]columns;
  List<T>rows = new ArrayList<>();


  /** 
   * 列定義を指定する
   * @param columns
   */
  public RowTableModel(@SuppressWarnings("unchecked") SgColumn<T, ?>...columns) {
    this.columns = columns;
  }

  /** 指定行を取得する */
  public T getRow(int index) {
    return rows.get(index);
  }

  /** 全行のストリームを取得する */
  public Stream<T>getRows() {
    return rows.stream();
  }

  /** 行データを追加する */
  public int add(T object) {
    int index = rows.size();
    insert(index, object);
    return index;
  }

  /** 行データを挿入する */
  public void insert(int index, T object) {
    rows.add(index, object);
    fireEvent(index, TableModelEvent.INSERT);
  }
  

  /** 全行を削除する */
  public void clear() {
    for (int i = rows.size() - 1; i >= 0; i--) {
      remove(i);
    }
  }
  
  /** 指定行を削除する */
  public void remove(int index) {
    rows.remove(index);
    fireEvent(index, TableModelEvent.DELETE);
  }

  /** 指定行データを更新する */
  public void update(int index, T object) {
    rows.set(index,  object);
    fireEvent(index, TableModelEvent.UPDATE);
  }

  /** イベントを発行する */
  private void fireEvent(int row, int type) {
    TableModelEvent e = new TableModelEvent(
        this, row, row, TableModelEvent.ALL_COLUMNS, type);    

    listeners.stream().forEach(l->l.tableChanged(e));
  }

  /** 行数を取得する */
  @Override
  public int getRowCount() {
    return rows.size();
  }

  /** 列数を取得する */
  @Override
  public int getColumnCount() {
    return columns.length;
  }

  /** 指定列名を取得する */
  @Override
  public String getColumnName(int columnIndex) {
    return (String)columns[columnIndex].getColumn().getHeaderValue();
  }

  /** 指定列データのクラスを取得する */
  @Override
  public Class<?> getColumnClass(int columnIndex) {
    return columns[columnIndex].columnClass;
  }

  /** セル値を取得する */
  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    return columns[columnIndex].getCellData(rows.get(rowIndex));
  }

  /** 
   * セル編集はできなくて良いのだが、
   * trueを返さないと「セルボタン」のようなものが機能しない
   */
  @Override
  public boolean isCellEditable(int rowIndex, int columnIndex) {
    return true;
  }
  
  /** セルデータ設定はできないが、受け入れないと「セルボタン」のようなものが機能しない */
  @Override
  public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

  }

  /** イベントリスナーリスト */
  private List<TableModelListener>listeners = new ArrayList<>();

  /** イベントリスナーを追加する */
  @Override
  public void addTableModelListener(TableModelListener l) {
    listeners.add(l);    
  }

  /** イベントリスナーを除去する */
  @Override
  public void removeTableModelListener(TableModelListener l) {
    listeners.remove(l);
  }
}
