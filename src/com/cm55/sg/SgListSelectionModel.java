package com.cm55.sg;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;

import javax.swing.*;

/**
 * テーブルその他用の選択モデル
 * <p>
 * 選択モード及び、選択イベントのハンドリングを行う。デフォルトは単一選択
 * になっている。
 * </p>
 * @author ysugimura
 */
public class SgListSelectionModel {

  /** 選択モデル */
  ListSelectionModel selectionModel;
  
  /** 
   * 現在の選択状態
   * {@link ListSelectionModel}のイベント発行がおかしいため、
   * 「現在の選択状態」を保持しておき、それとは異なる選択状態になったときに
   * 「選択イベント」を発行することにする。
   */
  Selection currentSelection;
  
  /** 選択ハンドラ登録 */
  Optional<Consumer<Selection>>selectionHandler = Optional.empty();

  final Supplier<int[]>getSelections;
  
  /** 
   * {@link ListSelectionModel}を与え初期化する。
   */
  SgListSelectionModel(
      ListSelectionModel selectionModel,
      Supplier<int[]>getSelections) {

    this.getSelections = getSelections;
    
    // 指定された選択モデルを保持し、単一行選択モードにする。
    this.selectionModel = selectionModel;
    selectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); 

    // 現在の選択状態を保持する
    currentSelection = new Selection(getSelections.get());
       
    // 選択イベントを発行するが、「以前とは異なる選択状態」になったときにだけ
    // イベントを発行する
    selectionModel.addListSelectionListener(e-> {
      
      // 調整モードを無視する
      if (e.getValueIsAdjusting()) return;
      
      // 新たな選択状態を取得する。以前と同じなら何もしない
      Selection newSelection = new Selection(getSelections.get());
      if (currentSelection.equals(newSelection)) return;
      
      
      // 現在の選択状態を置き換え、イベントを発行
      currentSelection = newSelection;
      selectionHandler.ifPresent(h->h.accept(currentSelection));
      changeSelectionAwares(currentSelection);
    });
  }

  /** 選択があるときに有効化されるべきコンポーネント */
  private List<SgComponent<?>>enabledAwares = new ArrayList<>();
  
  /**
   * 選択があるときに有効化されるべきコンポーネントを登録する。
   * これらは何も選択の無いときは無効化され、何かしら選択があるときは
   * 有効化される。
   * @param components 選択があるときに有効化されるべきコンポーネント
   * @return 本オブジェクト
   */
  public SgListSelectionModel setEnabledAwares(SgComponent<?>...components) {
    enabledAwares = Arrays.stream(components).collect(Collectors.toList());
    changeSelectionAwares(new Selection(getSelections.get()));
    return this;
  }

  /** 選択時有効化コンポーネントの状態を変更する */
  private void changeSelectionAwares(Selection selection) {
    boolean selected = selection.hasSelection();
    enabledAwares.stream().forEach(ea->ea.setEnabled(selected));
  }
  

  /**
   * 複数範囲選択モードを設定する
   * <p>
   * この選択モデルでの初期値は単一選択モードになっている。
   * このメソッドで複数範囲選択モードにすることができる。
   * </p>
   * <p>
   * ※本ライブラリでは、単一範囲選択モードはサポートしていない。
   * 単一選択か、複数範囲選択かのいずか。
   * </p>
   * @param value true：複数範囲選択モード、false:単一選択モード
   * @return 本オブジェクト
   */
  public SgListSelectionModel setMultiple(boolean value) {
    selectionModel.setSelectionMode(value?
        ListSelectionModel.MULTIPLE_INTERVAL_SELECTION:
        ListSelectionModel.SINGLE_SELECTION
    );
    return this;
  }
  
  /** 選択イベントハンドラを設定する */
  public SgListSelectionModel setSelectionHandler(Consumer<Selection>value) {
    this.selectionHandler = Optional.of(value);
    return this;
  }
  
  /**
   * 指定された行インデックスのみを選択状態にする。
   * 選択イベントが発生する。
   * @param index 新たに選択する単一インデックス
   */
  public void select(int index) {
    selectionModel.setSelectionInterval(index, index);
  }
  
  /**
   * 無選択状態にする。
   * 選択状態から無選択になった場合は選択イベントが発生する。
   */
  public void clear() {
    selectionModel.clearSelection();
  }
  
  /** 
   * 現在の選択状態を取得する。
   */
  public Selection getSelection() {
    return currentSelection;
  } 
  
  /** 
   * 選択状態
   * 選択インデックスを保持し、選択の有無、インデックスを提供する
   * @author ysugimura
   */
  public static class Selection {

    /** 選択インデックス */
    final int[]indices;

    /** 選択インデックスを指定する */
    Selection(int[]indices) {
      Arrays.sort(indices);
      this.indices = indices;
    }

    /**
     * 選択中の最小インデックスを取得する。
     * 選択の無い場合は-1を返す。
     * @return
     */
    public int getMinIndex() {
      return indices.length == 0? -1:indices[0];
    }

    /** 選択が無いか */
    public boolean isEmpty() {
      return indices.length == 0;
    }

    /** 選択があるか */
    public boolean hasSelection() {
      return indices.length > 0;
    }

    /** すべての選択インデックスを返す */
    public int[]getIndices() {
      return indices;
    }

    /** 他の選択状態と等しいか */
    @Override
    public boolean equals(Object o) {
      if (!(o instanceof Selection)) return false;
      Selection that = (Selection)o;
      return Arrays.equals(this.indices, that.indices);
    }

    /** デバッグ用文字列化 */
    @Override
    public String toString() {
      return "first:" + getMinIndex() + "," + Arrays.stream(getIndices())
            .mapToObj(i->i + "").collect(Collectors.joining(","));
    }
  }
}
