package com.cm55.sg;


import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.*;

import javax.swing.*;
import javax.swing.table.*;

/**
 * テーブル
 * @author ysugimura
 *
 * @param <T> テーブル行のオブジェクト型
 */
public class SgTable<T> extends SgComponent<SgTable<T>> {
  
  /** Swingの{@link JTable} */
  JTable table;
  
  /** ヘッダ */
  SgColumnModel<T> headers;
  
  /** 列定義配列 */
  SgColumn<T, ?>[]columns;
  
  
  RowTableModel<T> tableModel;

  
  SgListSelectionModel selectionModel;

  /**
   * 列を指定して作成する
   * @param columns 列定義
   */
  @SafeVarargs
  @SuppressWarnings({ "unchecked", "rawtypes" })
  public SgTable(SgColumn<T, ?>...columns) {
   this.columns = columns;
    headers = new SgColumnModel(columns);
    tableModel = new RowTableModel(columns);

    
    table = new JTable(tableModel, headers.headers);

    selectionModel = new SgListSelectionModel(
      table.getSelectionModel(),
      table::getSelectedRows
    );
  }

  /** 
   * 行ピクセル高さを指定する
   * @param height 行ピクセル高さ
   * @return 本オブジェクト
   */
  public SgTable<T> setRowHeight(int height) {
    table.setRowHeight(height);
    return this;
  }
  
  /** 
   * テーブルに行を追加する
   * @param row 追加する行
   * @return 追加されたインデックス
   */
  public int addRow(T row) {
    return tableModel.add(row);
  }

  /**
   * テーブルに行を追加し、その行を選択して表示する。
   */
  public int addRowAndShow(T row) {
    int index = addRow(row);
    selectionModel.select(index);
    this.scrollToVisible(index, 0);
    return index;
  }
  
  /** 
   * 指定インデックスの行を取得する
   * @param index 取得する行インデックス、０ベース
   * @return 取得された行
   */
  public T getRow(int index) {
    return tableModel.getRow(index);
  }

  /** 
   * 全行をストリームとして取得する
   * @return 全行のストリーム
   */
  public Stream<T>getRows() {
    return tableModel.getRows();
  }

  /** 
   * ストリームで示された行郡でテーブル内容を置き換える
   * @param rows 置き換える行ストリーム
   * @return 本オブジェクト
   */
  public SgTable<T>setRows(Stream<T>rows) {
    clearRows();
    rows.forEach(row->addRow(row));
    return this;
  }
  
  /** 
   * 全行を削除する。
   * 何らかの選択状態であれば、非選択イベントが発生する
   * @return 本オブジェクト
   */
  public SgTable<T>clearRows() {
    tableModel.clear();
    return this;
  }

  /** 
   * 指定インデックスの行を削除する
   * @param index 削除する行インデックス、０ベース
   */
  public void removeRow(int index) {
    tableModel.remove(index);
  }

  /**
   * 指定行を更新する
   * @param index 行インデックス
   * @param row 行データ
   */
  public void updateRow(int index, T row) {
    tableModel.update(index,  row);
  }
  
  /** 選択モデルを取得する */
  public SgListSelectionModel getSelectionModel() {
    return selectionModel;
  }

  /** もとの{@link JTable}を取得する */
  @Override
  public JTable w() {
    return table;
  }

  /**
   * 指定されたセルを表示するようにスクロールする。
   * 行のみが問題の場合は列インデックスを0にすればよい。
   * @param rowIndex 行インデックス
   * @param vColIndex 列インデックス
   */
  public void scrollToVisible(int row, int col) {
    table.scrollRectToVisible(new Rectangle(table.getCellRect(row, col, true)));
  }
  
  public static class SgColumnModel<T> {
    private SgColumn<T, ?>[]columns;
    public TableColumnModel headers;
    @SafeVarargs
    public SgColumnModel(SgColumn<T, ?>...columns) {
      this.columns = columns;
      headers = new DefaultTableColumnModel();
      int index = 0;
      for (SgColumn<T, ?>column: columns) {
        headers.addColumn(column.getColumn());
        column.getColumn().setModelIndex(index++);
      }
    }
    public SgColumn<T, ?>get(int index) {
      return columns[index];
    }    
  }
}
