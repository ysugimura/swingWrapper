package com.cm55.sg;

import java.awt.*;

public abstract class SgLayout {

  public SgLayout() {

  }

  public abstract void applyTo(Container container);
}
