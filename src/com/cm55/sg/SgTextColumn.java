package com.cm55.sg;

import java.util.function.*;

/**
 * テキストを表示する列
 * <p>
 * T型の行オブジェクト型から任意型のオブジェクトを取り出す。
 * 文字列に限らず整数等でも良いが、それは結局文字列として表示される。
 * </p>
 * @author ysugimura
 *
 * @param <T>
 */
public class SgTextColumn<T> extends SgColumn<T, SgTextColumn<T>> {

  Function<T, Object>cellExtractor;

  /**
   * 列タイトルとエクストラクタを指定する
   * @param title 列タイトル
   * @param cellExtractor 行オブジェクトから任意のデータを取得する。
   * 文字列に限らず、整数等でもよい。
   */
  public SgTextColumn(String title, Function<T, Object>cellExtractor) {
    super(title, String.class);
    this.cellExtractor = cellExtractor;
  }

  /**
   * 行オブジェクトから任意のデータを取得する
   */
  @Override
  public Object getCellData(T row) {
    return cellExtractor.apply(row);
  }  
}
