package com.cm55.sg;

import java.awt.*;
import java.util.*;

import javax.swing.*;

public class SgBoxLayout extends SgLayout {
  
  final int axis;
  final SgContainer<?>[]containers;
  
  private SgBoxLayout(int axis, SgContainer<?>...containers) {
    this.axis = axis;
    this.containers = containers;
  }

  @Override
  public void applyTo(Container container) {  
    BoxLayout layout = new BoxLayout(container, axis);
    container.setLayout(layout);
    
    Arrays.stream(containers).forEach(c-> {
      container.add(c.w());
    });
  }

  public static class H extends SgBoxLayout {
    public H(SgContainer<?>...containers) {
      super(BoxLayout.X_AXIS, containers);
    }
  }
  
  public static class V extends SgBoxLayout {
    public V(SgContainer<?>...containers) {
      super(BoxLayout.Y_AXIS, containers);
    }
  }
}
