package com.cm55.sg;

import javax.swing.*;
import javax.swing.GroupLayout.*;

public class SgForm extends SgComponent<SgPanel> {

  JPanel panel;
  
  private SgForm(JPanel panel) {
    this.panel = panel;
  }
  
  @Override
  public JComponent w() {
    return panel;
  }
  
  public static class Builder {
    private GroupLayoutBuilder builder = new GroupLayoutBuilder();
    
    public Builder add(String title, SgComponent<?> component) {
      builder.addRow(Alignment.BASELINE, new JLabel(title), component.w());
      return this;
    }
    
    public SgForm build() {
      return new SgForm(builder.build(new JPanel()));
    }
  }
}
