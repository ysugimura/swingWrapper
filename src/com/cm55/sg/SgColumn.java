package com.cm55.sg;

import javax.swing.table.*;

/**
 * テーブル用列定義
 * <p>
 * 本クラスは抽象クラスであり、直接使用されることはない
 * </p>
 * @author ysugimura
 *
 * @param <R>
 */
public abstract class SgColumn<R, T extends SgColumn<R, T>> {

  protected TableColumn column;
  public final Class<?>columnClass;
  
  public SgColumn(String title, Class<?>columnClass) {
    column = new TableColumn();
    column.setHeaderValue(title);
    this.columnClass = columnClass;
  }
  
  @SuppressWarnings("unchecked")
  public T setPreferredWidth(int width) {
    column.setPreferredWidth(width);
    return (T)this;
  }

  public abstract Object getCellData(R row);

  public TableColumn getColumn() {
    return column;
  }
  
  public void clicked() {}
  
}
