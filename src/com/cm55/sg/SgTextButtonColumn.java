package com.cm55.sg;

import java.util.function.*;

import javax.swing.*;

/**
 * テキストをラベルとするボタン列
 * @author ysugimura
 *
 * @param <R> 行のオブジェクトタイプ
 */
public class SgTextButtonColumn<R> extends AbstractButtonColumn<R, SgTextButtonColumn<R>> {  
  
  /** 行オブジェクトからテキストボタンラベルを取得するコールバック */
  private final Function<R, String>labelGetter;

  /**
   * 列タイトル、クリック時のコールバック、行からボタンラベルを取得するコールバックを指定する。
   * @param title 列タイトル
   * @param labelGetter 行からボタンテキストを取得するコールバック
   */
  public SgTextButtonColumn(String title, Function<R, String>labelGetter) {
    super(title);
    this.labelGetter = labelGetter;
  }

  /**
   * 指定された行に対するセルデータを取得する。
   * ここではコンテキストを返す。実際のセルの描画は上位のレンダラとエディタによってなされ、ここで返されたものが直接描画されるわけではない。
   */
  @Override
  public Object getCellData(R row) {
    return new Context<R>(clicked, labelGetter.apply(row));
  }  

  /**
   * テキストをラベルとするボタン列用のコンテキスト
   * @param <R>　行のオブジェクトタイプ
   */
  private static class Context<R> extends AbstractContext {
    final String label;
    
    Context(Consumer<Integer>clicked, String label) {
      super(clicked);
      this.label = label;
    }    
    
    /** ボタンの状態を変更する */
    @Override
    protected void changeButtonState(JTable table, JButton button, boolean isSelected) {
      if (isSelected) {
        button.setForeground(table.getSelectionForeground());
        button.setBackground(table.getSelectionBackground());
      } else {
        button.setForeground(table.getForeground());
        button.setBackground(table.getBackground());
      }
      button.setText(label);
    }
  }
}
