package com.cm55.sg;

import java.util.function.*;

import javax.swing.*;

public class SgMenuItem extends AbstractMenuItem<SgMenuItem> {

  protected JMenuItem menuItem;
  
  public SgMenuItem(String text, Consumer<SgMenuItem>handler) {
    menuItem = new JMenuItem(text);
    if (handler != null)
      menuItem.addActionListener(e->handler.accept(this));
  }

  public String getText() {
    return menuItem.getText();
  }
  
  public SgMenuItem setMnumonic(char c) {
    menuItem.setMnemonic(c);
    return this;
  }
  
  @Override
  public JComponent w() {
    return menuItem;
  }
}
