package com.cm55.sg;

public class SgLaf {

  public final String name;
  public final String clazz;
  
  private SgLaf(String name, String clazz) {
    this.name = name;
    this.clazz = clazz;
  }
  
  /* ORACLE =================================================================*/
  
  /** Nimbus LAF */
  public static final SgLaf NIMBUS = 
      new SgLaf("Nimbus", "javax.swing.plaf.nimbus.NimbusLookAndFeel");

  /** Motif LAF */
  public static final SgLaf MOTIF = 
      new SgLaf("Motif", "com.sun.java.swing.plaf.motif.MotifLookAndFeel");
  
  
  /** Metal LAF */
  public static final SgLaf METAL = 
      new SgLaf("Metal", "javax.swing.plaf.metal.MetalLookAndFeel");
  
  /** GTK LAF */
  public static final SgLaf GTK = 
      new SgLaf("Gtk", "com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
  
  /** Windows LAF */
  public static final SgLaf WINDOWS = 
      new SgLaf("Windows", "com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
  
  
  /* THIRD PARTY ============================================================*/
  
  /** Napkin LAF */
  public static final SgLaf NAPKIN = 
      new SgLaf("Napkin", "net.sourceforge.napkinlaf.NapkinLookAndFeel");

  /** Web LAF */
  public static final SgLaf WEB = 
      new SgLaf("Web", "com.alee.laf.WebLookAndFeel");
  
  /** Quaqua */
  public static final SgLaf QUAQUA =
      new SgLaf("Quaqua", "ch.randelshofer.quaqua.QuaquaLookAndFeel");
  
  /*
   *  sun/swing/plaf/synth/SynthUIを参照しているためJava9以上で使用不可
  public static final SgLaf SEAGLASS = 
      new SgLaf("SeaGlass", "com.seaglasslookandfeel.SeaGlassLookAndFeel");
  
   */   
}
