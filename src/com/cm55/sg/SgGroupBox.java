package com.cm55.sg;

import javax.swing.*;

/**
 * グループボックス
 * <p>
 * 子コンポーネントのグループ化表示を行うために便利なコンポーネント。
 * 内部的には、パネルにタイトルボーダーにつけたものであり、
 * パネルに適用可能なレイアウトはすべて使うことができる。
 * </p>
 * @author ysugimura
 */
public class SgGroupBox extends SgPanel {

  public SgGroupBox(String title, SgLayout layout) {
    super(layout);
    setBorder(title);
  }
  
  public SgGroupBox(String title, SgComponent<?>component) {
    super(new SgBorderLayout.V(null,  component, null));    
    setBorder(title);
  }
  
  void setBorder(String title) {
    panel.setBorder(BorderFactory.createTitledBorder(title));    
  }

}
